﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Const
{
    // 最終ウェーブ数
    public const int LAST_WAVE = 6;

    // 最大ライフ
    public const int MAX_LIFE = 8;

    // パワーショット閾値
    public const int POWER_SHOT = 300;
}
