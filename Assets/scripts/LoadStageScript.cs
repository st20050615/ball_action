﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadStageScript : MonoBehaviour {

    private int stage;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setStage(int stage)
    {
        this.stage = stage;
    }

    public void LoadStage()
    {
        Data.Instance.stage = stage;
        Data.Instance.wave = 1;
        Data.Instance.score = 0;
        Data.Instance.scores = new int[Const.LAST_WAVE];
        Data.Instance.life = Const.MAX_LIFE;
        Debug.Log("load stage: " + stage);
        SceneManager.LoadScene("stage");
    }

}
