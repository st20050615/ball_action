﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineScript : MonoBehaviour {

	private LineRenderer renderer;

	// Use this for initialization
	void Start () {
		renderer = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void drow(Vector3 touchStartPos, Vector3 touchNowPos) {

        float lineWidth = 0.1f;
		Color lineColor = Color.magenta;
        if (Vector2.Distance(touchStartPos, touchNowPos) > 1) {
            lineWidth = 0.3f;
        }

        if (Vector2.Distance(touchStartPos, touchNowPos) > Const.POWER_SHOT) {
            lineColor = Color.red;
        }

		// 線の幅
		renderer.SetWidth(0.1f, lineWidth);
		// 頂点の数
		renderer.SetVertexCount(2);

		renderer.SetColors(lineColor, lineColor);

        Camera gameCamera = Camera.main;
        touchStartPos = gameCamera.ScreenToWorldPoint( touchStartPos );
        touchNowPos = gameCamera.ScreenToWorldPoint( touchNowPos );

		// 頂点を設定
		renderer.SetPosition(0, new Vector2(touchStartPos.x, touchStartPos.y));
		renderer.SetPosition(1, new Vector2(touchNowPos.x, touchNowPos.y));
	}

	public void clear() {
		renderer.SetVertexCount(0);
	}
}