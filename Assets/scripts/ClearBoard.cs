﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ClearBoard : MonoBehaviour {

    public Text ScoreText;
    
    void Awake() {
        int totalScore = 0;
        GameObject GridPanel = GameObject.Find("GridPanel");
        int count = 0;
        foreach (Transform child in GridPanel.transform)
        {
            Text text = child.transform.GetChild(0).GetComponent<Text>();
            if (Data.Instance.scores[count] != 0)
            {
                text.text = Data.Instance.scores[count].ToString();
                totalScore += Data.Instance.scores[count];
            }
            count++;
        }
        ScoreText.text = totalScore.ToString();
    }

    // Use this for initialization
    void Start () {
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void NextPage()
    {
        if (Data.Instance.wave == Const.LAST_WAVE)
        {
            this.TitleBack();
        } else
        {
            this.NextStage();
        }
    }

    private void TitleBack()
    {
        SceneManager.LoadScene("stage_select");
    }

    private void NextStage()
    {
        Data.Instance.wave = Data.Instance.wave + 1;
        Data.Instance.score = 0;
        SceneManager.LoadScene("stage");
    }

}
