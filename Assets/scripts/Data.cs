﻿using UnityEngine;
using System.Collections;

public class Data
{
    public readonly static Data Instance = new Data();

    // 現在のスコア合計
    public int score = 0;

    // 現在のスコア
    public int[] scores;

    // 現在のステージ
    public int stage = 1;
    // 現在のウェーブ
    public int wave = 1;
    // 現在のライフ
    public int life = Const.MAX_LIFE;
}