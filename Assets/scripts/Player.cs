﻿using GodTouches;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	public float flyForce; //ジャンプ力
	public GameObject line;

    // CanvasのUI
    private GameObject lifePanel;
    private Text scoreText;

	private Vector3 touchStartPos;
	private Vector3 touchEndPos;
	private float directionX;
	private float directionY;
    private float distance;
    private float aim;
	private bool shot = false;
    private bool shotStart = false;
    private short shotTimer = 0;
	private LineScript script;
	
	//OverlapAreaで絞る為のレイヤーマスクの変数
    public LayerMask whatIsGround;

	[Range(0,1)] public float sliding = 0.9f; //スライドモーション

    void Awake()
    {
		script = line.GetComponent<LineScript>();
        
        lifePanel = GameObject.Find("StatusPanel1");

        for (int i = 0; i < Data.Instance.life; i++)
        {
            GameObject LifeImage = (GameObject)Resources.Load ("Prefabs/LifeImage");
            GameObject _lifeImage = Instantiate(LifeImage);
            _lifeImage.transform.SetParent(this.lifePanel.transform);
        }

        scoreText = GameObject.Find("StatusPanel2").transform.GetChild(0).GetComponent<Text>();
        scoreDisplay();
    }

    // Use this for initialization
    void Start () {
    }

	void Update() {
        var phase = GodTouch.GetPhase();
        // 入力なし
        if (phase == GodPhase.None)
        {
            //Debug.Log("no click");
            script.clear();
        }

        // 入力開始
        if (phase == GodPhase.Began)
        {
            Debug.Log("click");
            touchStartPos = GodTouch.GetPosition();
        }

        // 入力終了
        if (phase == GodPhase.Ended)
        {
            Debug.Log("click up");
            touchEndPos = GodTouch.GetPosition();
            GetDirection();
        }

        if (phase == GodPhase.Moved)
        {
            Debug.Log("clicked");
            Vector3 touchNowPos = GodTouch.GetPosition();
            script.drow(touchStartPos, touchNowPos);
        }


    }

    void FixedUpdate() {
        //プレイヤー位置
        Vector2 pos = transform.position;
        //あたり判定四角領域の中心点
        Vector2 groundCheck = new Vector2(pos.x, pos.y - (GetComponent<CircleCollider2D>().radius)*1.5f);
        //あたり判定四角領域の範囲の幅
        Vector2 groundArea = new Vector2(GetComponent<CircleCollider2D>().radius * 0.45f, 0.05f);

        //あたり判定四角領域の範囲
        bool grounded = Physics2D.OverlapArea(groundCheck + groundArea, groundCheck - groundArea,whatIsGround);
        //Animatorパラメーターの設定
        //GetComponent<Animator>().SetBool("Ground", grounded);

        if (shot && grounded)
        {
            shotStart = true;
        }

        if (shotStart)
        {
            shotTimer++;
        } else
        {
            shotTimer = 0;
        }

        //ジャンプ
        if (shotStart && shotTimer >= 4) {
            if (Data.Instance.life > 0)
            {
                Data.Instance.life--;
                Vector2 v;
                v.x = Mathf.Cos (Mathf.Deg2Rad * aim);
                v.y = Mathf.Sin (Mathf.Deg2Rad * aim);
                float force = distance > Const.POWER_SHOT ? flyForce * 2 : flyForce;
                Debug.Log("force:" + force);
                GetComponent<Rigidbody2D>().AddForce(v * force, ForceMode2D.Impulse);
                shot = false;
                shotStart = false;
                Data.Instance.score++;
                scoreDisplay();
            }
        }

        lifeDisplay();

	}

	void GetDirection(){
		// フリックの移動座標を取得
    	directionX = (touchEndPos.x - touchStartPos.x) * -1;
    	directionY = (touchEndPos.y - touchStartPos.y) * -1;
        distance = Vector2.Distance(touchStartPos, touchEndPos);
        //Debug.Log("directionX:" + directionX);
        //Debug.Log("directionY:" + directionY);
        //Debug.Log("distance:" + distance);
        aim = Mathf.Rad2Deg * Mathf.Atan2(directionY, directionX);
		if (directionX != 0 && directionY != 0) {
			shot = true;
		}
	}

    private void scoreDisplay()
    {
        scoreText.text = "Stage:" + Data.Instance.stage + "-" + Data.Instance.wave + " Count:" + Data.Instance.score;
    }

    private void lifeDisplay()
    {
        int lifeObjectCount = lifePanel.transform.childCount;
        if (lifeObjectCount > Data.Instance.life)
        {
            Destroy(lifePanel.transform.GetChild(0).gameObject);
        } else if (lifeObjectCount < Data.Instance.life)
        {
            GameObject LifeImage = (GameObject)Resources.Load ("Prefabs/LifeImage");
            GameObject _lifeImage = Instantiate(LifeImage);
            _lifeImage.transform.SetParent(this.lifePanel.transform);
        }
    }

    // ライフ回復
    private void lifeRecover()
    {
        if (Data.Instance.life < Const.MAX_LIFE)
        {
            Data.Instance.life++;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("destory");
        if (other.tag == "Star")
        {
            Destroy(other.gameObject);
            Invoke("checkStar", 0.1f);
        }
        if (other.tag == "Goal")
        {
            GetComponent<CircleCollider2D>().isTrigger = true;
        }
        if (other.tag == "ClearPoint")
        {
            //SaveHighScore();
            lifeRecover();
            Data.Instance.scores[Data.Instance.wave - 1] = Data.Instance.score;
            SceneManager.LoadScene("clear");
        }

    }

    private void checkStar()
    {
        GameObject[] stars = GameObject.FindGameObjectsWithTag("Star");
        lifeRecover();
        Debug.Log("stars:" + stars.Length);
        if (stars.Length == 0)
        {
            Destroy(GameObject.FindGameObjectsWithTag("Hole_Def")[0]);
        }

    }

    private void SaveHighScore()
    {
        if (PlayerPrefs.GetInt("Data") == 0 || PlayerPrefs.GetInt("Data") > Data.Instance.score)
        {
            PlayerPrefs.SetInt("Data", Data.Instance.score);
        }
    }

    public void StageSelectBack()
    {
        Data.Instance.score = 0;
        SceneManager.LoadScene("stage_select");
    }
}
