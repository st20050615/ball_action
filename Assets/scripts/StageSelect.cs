﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StageSelect : MonoBehaviour {

    private int stage = 6;

    void Awake() {
        GameObject uiStage = (GameObject)Resources.Load ("Prefabs/UIStage");
		for (int i = 1; i <= stage; i++)
        {
            GameObject _uiStage = Instantiate(uiStage);
            _uiStage.transform.SetParent(this.transform);
            Text text = _uiStage.transform.GetChild(0).GetComponent<Text>();
            text.text = "Stage" + i;
            _uiStage.GetComponent<LoadStageScript>().setStage(i);
        }
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TitleBack()
    {
        SceneManager.LoadScene("title");
    }

}
