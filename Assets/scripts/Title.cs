﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Title : MonoBehaviour {

    // Use this for initialization
    void Start () {
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void GameStart()
    {
        Data.Instance.score = 0;
        Data.Instance.scores = new int[Const.LAST_WAVE];
        SceneManager.LoadScene("stage_select");
    }

    public void ResetHighScore()
    {
        PlayerPrefs.DeleteKey("Data");
    }
}
