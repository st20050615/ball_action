﻿using UnityEngine;
using System.Collections;
using System.IO;

public class MapGenerator : MonoBehaviour
{
    public GameObject stageObj;
    // 壁のプレハブ
    public GameObject wallPrefab;

    // アイテムのプレハブ
    public GameObject starPrefab;

    // 三角壁（右向き）のプレハブ
    public GameObject tryRightPrefab;

    // 三角壁（左向き）のプレハブ
    public GameObject tryLeftPrefab;

    // グリーンのプレハブ
    public GameObject greenPrefab;

    // ゴールのプレハブ
    public GameObject goalPrefab;

    private int start_x = -9;
    private int start_y = 5;
    private TextAsset textFile;

    // Use this for initialization
    void Start()
    {

        //これがマップの元になるデータ
        //string map_matrix  = "1011111111111111111:1000000000000000001:1000000000000000001:1000000000000000001:1000000000000000001:1000000000000000001:1000000000000000001:1000000000000000001:1000000000000000001:1000000000000000001:1111111111111111111";
        string map_matrix = "";
        textFile = Resources.Load("MapData/map" + Data.Instance.stage + "-" + Data.Instance.wave) as TextAsset;
        StringReader reader = new StringReader(textFile.text);

        while (reader.Peek() > -1)
        {
            string line = reader.ReadLine();
            Debug.Log(line);
            map_matrix += line + ':';
        }

        // 引数にこれを入れてマップ生成する
        CreateMap(map_matrix);
    }

    //マップを作るメソッド
    void CreateMap(string map_matrix)
    {
        //「:」をデリミタとして、mapp_matrix_arrに配列として分割していれます
        string[] map_matrix_arr = map_matrix.Split(':');

        //map_matrix_arrの配列の数を最大値としてループ
        for (int y = 0; y < map_matrix_arr.Length; y++)
        {
            //xを元に配列の要素を取り出す
            string x_map = map_matrix_arr[y];
            //１配列に格納されている文字の数でx軸をループ
            for (int x = 0; x < x_map.Length; x++)
            {
                //配列から取り出した１要素には022520000000000000こんな値が入っているのでこれを１文字づつ取り出す
                int obj = int.Parse(x_map.Substring(x, 1));

                //もしも1だったら壁ということで壁のプレハブをインスタンス化してループして出したx座標を指定して設置
                if (obj == 1)
                {
                    GameObject _obj = Instantiate(wallPrefab, new Vector2(start_x + x, start_y - y), Quaternion.identity);
                    _obj.transform.parent = stageObj.transform;

                }

                // アイテム
                if (obj == 2)
                {
                    GameObject _obj = Instantiate(starPrefab, new Vector2(start_x + x, start_y - y), Quaternion.identity);
                    _obj.transform.parent = stageObj.transform;
                }

                // 三角壁（右向き）
                if (obj == 3)
                {
                    GameObject _obj = Instantiate(tryRightPrefab, new Vector2(start_x + x, start_y - y), Quaternion.identity);
                    _obj.transform.parent = stageObj.transform;
                }

                // 三角壁（左向き）
                if (obj == 4)
                {
                    GameObject _obj = Instantiate(tryLeftPrefab, new Vector2(start_x + x, start_y - y), Quaternion.Euler(0, 180, 0));
                    _obj.transform.parent = stageObj.transform;
                }

                // グリーン
                if (obj == 8)
                {
                    GameObject _obj = Instantiate(greenPrefab, new Vector2(start_x + x, start_y - y), Quaternion.identity);
                    _obj.transform.parent = stageObj.transform;
                }

                // ゴール
                if (obj == 9)
                {
                    GameObject _obj = Instantiate(goalPrefab, new Vector2(start_x + x, start_y - y), Quaternion.identity);
                    _obj.transform.parent = stageObj.transform;
                }

            }
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}